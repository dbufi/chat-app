module.exports = {
  "plugins": ["react", "react-hooks", "jsx-a11y", "import"],
  "settings": {
    "import/resolver": {
      "node": {
        "paths": ["src"],
        "extensions": [".js", ".jsx"]
      }
    },
    "react": {
      "version": "latest"
    },
    "jsx": true
  },
  "rules": {
    "no-confusing-arrow": 0,
    "jsx-a11y/no-noninteractive-element-interactions": 0,
    "prefer-destructuring": 0,
    "jsx-a11y/click-events-have-key-events": 0,
    "jsx-a11y/no-static-element-interactions": 0,
    "camelcase": 0,
    "no-return-assign": 0,
    "class-methods-use-this": 0,
    "max-len": 0,
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "warn",
    "import/no-named-as-default": 0,
    "import/no-named-as-default-member": 0,
    "import/prefer-default-export": 0,
    "no-underscore-dangle": 0,
    "no-plusplus": 0,
    "eol-last": 0,
    "arrow-body-style": 0,
    "no-param-reassign": 0,
    "consistent-return": 0,
    "padding-line-between-statements": [
      "error",
      { "blankLine": "always", "prev": "block-like", "next": "const" },
      { "blankLine": "always", "prev": "block-like", "next": "let" },
      { "blankLine": "always", "prev": "*", "next": "export" },
      { "blankLine": "always", "prev": "block-like", "next": "function" },
      { "blankLine": "always", "prev": "block-like", "next": "class" },
      { blankLine: "always", prev: "*", next: "return" }
    ]
  },
  "parser": "babel-eslint",
  "parserOptions": {
    "sourceType": "module",
  },
  "env": {
    "browser": true,
    "jest": true
  },
  "globals": {
    "shallow": true,
    "render": true,
    "mount": true
  },
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:jsx-a11y/recommended",
    "airbnb-base"
  ]
}