import axios from 'axios';
import baseUrl from '../Constants/BaseUrl';

const setNewPassword = (id, password) => {
  const url = `${baseUrl}auth/password/`;

  return axios
    .put(url,
      {
        id,
        password,
      });
};

export default setNewPassword;