import * as actionTypes from '../ActionTypes';
import { updateObject } from '../../Shared/Utils';

const initialState = {
  users: [],
  error: null,
  loading: false,
};

const usersListStart = (state) => {
  return updateObject(state, { error: null, loading: true });
};

const usersListSuccess = (state, action) => {
  return updateObject(state, {
    users: action.users,
    error: null,
    loading: false,
  });
};

const usersListFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};


const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.USERS_LIST_START:
      return usersListStart(state, action);
    case actionTypes.USERS_LIST_SUCCESS:
      return usersListSuccess(state, action);
    case actionTypes.USERS_LIST_FAIL:
      return usersListFail(state, action);

    default:
      return state;
  }
};

export default reducer;
