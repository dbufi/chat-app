import * as actionTypes from '../ActionTypes';
import { updateObject } from '../../Shared/Utils';

const initialState = {
  error: null,
  loading: false,
  response: null,
};

const emailStart = (state) => {
  return updateObject(state, { error: null, loading: true, response: null });
};

const emailSuccess = (state, action) => {
  return updateObject(state, {
    response: action.message,
    error: null,
    loading: false,
  });
};

const emailFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};


const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.EMAIL_START:
      return emailStart(state, action);
    case actionTypes.EMAIL_SUCCESS:
      return emailSuccess(state, action);
    case actionTypes.EMAIL_FAIL:
      return emailFail(state, action);

    default:
      return state;
  }
};

export default reducer;
