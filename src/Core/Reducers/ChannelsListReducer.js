import * as actionTypes from '../ActionTypes';
import { updateObject } from '../../Shared/Utils';

const initialState = {
  channels: [],
  error: null,
  loading: false,
};

const channelsListStart = (state) => {
  return updateObject(state, { error: null, loading: true });
};

const channelsListSuccess = (state, action) => {
  return updateObject(state, {
    channels: action.channels,
    error: null,
    loading: false,
  });
};

const channelsListFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};

const sendMessageStart = (state) => {
  return updateObject(state, { error: null, loading: true });
};

const sendMessageSuccess = (state, action) => {
  const channels = state.channels.map((item) => {
    if (item._id === action.newMessage.channel._id) {
      item.lastMessage = action.newMessage;
    }

    return item;
  });

  return {
    channels,
    error: null,
    loading: false,
  };
};

const sendMessageFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};


const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHANNELS_LIST_START:
      return channelsListStart(state, action);
    case actionTypes.CHANNELS_LIST_SUCCESS:
      return channelsListSuccess(state, action);
    case actionTypes.CHANNELS_LIST_FAIL:
      return channelsListFail(state, action);
    case actionTypes.SEND_MESSAGE_START:
      return sendMessageStart(state, action);
    case actionTypes.SEND_MESSAGE_SUCCESS:
      return sendMessageSuccess(state, action);
    case actionTypes.SEND_MESSAGE_FAIL:
      return sendMessageFail(state, action);

    default:
      return state;
  }
};

export default reducer;
