import * as actionTypes from '../ActionTypes';
import { updateObject } from '../../Shared/Utils';

const initialState = {
  user: {},
  error: null,
  loading: false,
  isAuth: false,
};

const authStart = (state) => {
  return updateObject(state, { error: null, loading: true });
};

const authSuccess = (state, action) => {
  return updateObject(state, {
    user: action.userData,
    error: null,
    loading: false,
    isAuth: true,
  });
};


const authFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};

const authLogout = (state) => {
  return updateObject(state, { user: null, isAuth: false });
};

const editProfileStart = (state) => {
  return updateObject(state, { error: null, loading: true });
};

const editProfileSuccess = (state, action) => {
  return updateObject(state, {
    user: action.userData,
    error: null,
    loading: false,
    isAuth: true,
  });
};

const editProfileFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    isAuth: false,
  });
};


const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_START:
      return authStart(state, action);
    case actionTypes.AUTH_SUCCESS:
      return authSuccess(state, action);
    case actionTypes.AUTH_FAIL:
      return authFail(state, action);
    case actionTypes.USER_LOGOUT:
      return authLogout(state, action);
    case actionTypes.EDIT_PROFILE_START:
      return editProfileStart(state, action);
    case actionTypes.EDIT_PROFILE_SUCCESS:
      return editProfileSuccess(state, action);
    case actionTypes.EDIT_PROFILE_FAIL:
      return editProfileFail(state, action);
    default:
      return state;
  }
};

export default reducer;
