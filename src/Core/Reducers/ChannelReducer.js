import * as actionTypes from '../ActionTypes';
import { updateObject } from '../../Shared/Utils';

const initialState = {
  channel: [],
  error: null,
  loading: false,
};

const channelStart = (state) => {
  return updateObject(state, { error: null, loading: true });
};

const channelSuccess = (state, action) => {
  return {
    channel: [...state.channel, action.channel],
    error: null,
    loading: false,
  };
};

const channelFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};


const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHANNEL_START:
      return channelStart(state, action);
    case actionTypes.CHANNEL_SUCCESS:
      return channelSuccess(state, action);
    case actionTypes.CHANNEL_FAIL:
      return channelFail(state, action);

    default:
      return state;
  }
};

export default reducer;
