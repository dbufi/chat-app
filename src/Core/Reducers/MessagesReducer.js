import * as actionTypes from '../ActionTypes';
import { updateObject } from '../../Shared/Utils';

const initialState = {
  channelId: '',
  messages: [],
  error: null,
  loading: false,
};

const messagesStart = (state) => {
  return updateObject(state, { error: null, loading: true });
};

const messagesSuccess = (state, action) => {
  return updateObject(state, {
    channelId: action.channelId,
    messages: action.messages,
    error: null,
    loading: false,
  });
};

const messagesFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};

const sendMessageStart = (state) => {
  return updateObject(state, { error: null, loading: true, success: false });
};

const sendMessageSuccess = (state, action) => {
  if (action.newMessage.channel._id !== state.channelId) {
    return state;
  }

  return {
    channelId: state.channelId,
    messages: [...state.messages, action.newMessage],
  };
};

const sendMessageFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    success: false,
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.MESSAGES_START:
      return messagesStart(state, action);
    case actionTypes.MESSAGES_SUCCESS:
      return messagesSuccess(state, action);
    case actionTypes.MESSAGES_FAIL:
      return messagesFail(state, action);
    case actionTypes.SEND_MESSAGE_START:
      return sendMessageStart(state, action);
    case actionTypes.SEND_MESSAGE_SUCCESS:
      return sendMessageSuccess(state, action);
    case actionTypes.SEND_MESSAGE_FAIL:
      return sendMessageFail(state, action);
    default:
      return { ...state };
  }
};

export default reducer;
