import * as actionTypes from '../ActionTypes';
import { updateObject } from '../../Shared/Utils';

const initialState = {
  updatedChannel: [],
  error: null,
  loading: false,
};

const editChannelStart = (state) => {
  return updateObject(state, { error: null, loading: true });
};

const editChannelSuccess = (state, action) => {
  return updateObject(state, {
    updatedChannel: action.channel,
    error: null,
    loading: false,
  });
};

const editChannelFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};


const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.EDIT_CHANNEL_START:
      return editChannelStart(state, action);
    case actionTypes.EDIT_CHANNEL_SUCCESS:
      return editChannelSuccess(state, action);
    case actionTypes.EDIT_CHANNEL_FAIL:
      return editChannelFail(state, action);

    default:
      return state;
  }
};

export default reducer;
