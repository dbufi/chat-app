import {
  createStore, applyMiddleware, compose, combineReducers,
} from 'redux';
import thunk from 'redux-thunk';
import RegisterReducer from '../Reducers/RegisterReducer';
import AuthReducer from '../Reducers/AuthReducer';
import EmailReducer from '../Reducers/EmailReducer';
import UserListReducer from '../Reducers/UsersListReducer';
import ChannelReducer from '../Reducers/ChannelReducer';
import ChannelListReducer from '../Reducers/ChannelsListReducer';
import EditChannelReducer from '../Reducers/EditChannelReducer';
import MessagesReducer from '../Reducers/MessagesReducer';

const composeEnhancers = process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;

const rootReducer = combineReducers({
  register: RegisterReducer,
  auth: AuthReducer,
  verifyEmail: EmailReducer,
  users: UserListReducer,
  channel: ChannelReducer,
  channels: ChannelListReducer,
  editChannel: EditChannelReducer,
  messages: MessagesReducer,
});

export const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk),
));
