import axios from 'axios';
import * as actionTypes from '../../ActionTypes';
import baseUrl from '../../../Constants/BaseUrl';
import { readFromStorage } from '../../../Constants/StorageManager';


export const editChannelStart = () => {
  return {
    type: actionTypes.EDIT_CHANNEL_START,
  };
};

export const editChannelSuccess = (channel) => {
  return {
    type: actionTypes.EDIT_CHANNEL_SUCCESS,
    channel,
  };
};

export const editChannelFail = (error) => {
  return {
    type: actionTypes.EDIT_CHANNEL_FAIL,
    error,
  };
};

export const editChannel = (
  _id,
  title,
  members,
) => {
  return (dispatch) => {
    dispatch(editChannelStart());
    const editChannelData = {
      _id,
      title,
      members,
    };
    const userToken = readFromStorage('token');
    const token = JSON.parse(userToken);
    const url = `${baseUrl}channels`;
    axios({
      method: 'patch',
      url,
      headers: { Authorization: `Bearer ${token}` },
      data: editChannelData,
    }).then((response) => {
      dispatch(editChannelSuccess(response.data));
    })
      .catch((error) => {
        dispatch(editChannelFail(error.response.data));
      });
  };
};