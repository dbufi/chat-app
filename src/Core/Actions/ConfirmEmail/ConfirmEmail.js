import axios from 'axios';
import * as actionTypes from '../../ActionTypes';
import baseUrl from '../../../Constants/BaseUrl';

export const emailStart = () => {
  return {
    type: actionTypes.EMAIL_START,
  };
};

export const emailSuccess = (response) => {
  return {
    type: actionTypes.EMAIL_SUCCESS,
    message: response,
  };
};

export const emailFail = (error) => {
  return {
    type: actionTypes.EMAIL_FAIL,
    error,
  };
};

export const confirmEmail = (email) => {
  return (dispatch) => {
    dispatch(emailStart());

    const url = `${baseUrl}auth/new_password`;

    axios
      .get(url,
        {
          params: {
            email,
          },
        })
      .then((response) => {
        dispatch(emailSuccess(response.data));
      })
      .catch((err) => {
        dispatch(emailFail(err.response.data.message));
      });
  };
};
