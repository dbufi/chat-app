import axios from 'axios';
import * as actionTypes from '../../ActionTypes';
import baseUrl from '../../../Constants/BaseUrl';
import { readFromStorage } from '../../../Constants/StorageManager';
import { getHttpErrorMsg } from '../../../Shared/Utils';

export const messagesStart = () => {
  return {
    type: actionTypes.MESSAGES_START,
  };
};

export const messagesSuccess = (messages, channelId) => {
  return {
    type: actionTypes.MESSAGES_SUCCESS,
    messages,
    channelId,
  };
};

export const messagesFail = (error) => {
  return {
    type: actionTypes.MESSAGES_FAIL,
    error,
  };
};

export const messages = (id) => {
  return (dispatch) => {
    dispatch(messagesStart());

    const userToken = readFromStorage('token');
    const token = JSON.parse(userToken);

    const url = `${baseUrl}messages`;
    axios({
      method: 'get',
      url,
      headers: { Authorization: `Bearer ${token}` },
      params: {
        channelId: id,
      },
    }).then((response) => {
      dispatch(messagesSuccess(response.data, id));
    })
      .catch((error) => {
        dispatch(messagesFail(getHttpErrorMsg(error)));
      });
  };
};
