import axios from 'axios';
import * as actionTypes from '../../ActionTypes';
import baseUrl from '../../../Constants/BaseUrl';
import { readFromStorage } from '../../../Constants/StorageManager';
import { getHttpErrorMsg } from '../../../Shared/Utils';

export const channelsListStart = () => {
  return {
    type: actionTypes.CHANNELS_LIST_START,
  };
};

export const channelsListSuccess = (channels) => {
  return {
    type: actionTypes.CHANNELS_LIST_SUCCESS,
    channels,
  };
};

export const channelsListFail = (error) => {
  return {
    type: actionTypes.CHANNELS_LIST_FAIL,
    error,
  };
};

export const channelList = () => {
  return (dispatch) => {
    dispatch(channelsListStart());

    const userToken = readFromStorage('token');
    const token = JSON.parse(userToken);

    const url = `${baseUrl}channels`;
    axios({
      method: 'get',
      url,
      headers: { Authorization: `Bearer ${token}` },
    }).then((response) => {
      dispatch(channelsListSuccess(response.data));
    })
      .catch((error) => {
        dispatch(channelsListFail(getHttpErrorMsg(error)));
      });
  };
};