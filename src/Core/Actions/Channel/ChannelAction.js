import axios from 'axios';
import * as actionTypes from '../../ActionTypes';
import baseUrl from '../../../Constants/BaseUrl';
import { readFromStorage } from '../../../Constants/StorageManager';
import { getHttpErrorMsg } from '../../../Shared/Utils';

export const channelStart = () => {
  return {
    type: actionTypes.CHANNEL_START,
  };
};

export const channelSuccess = (channel) => {
  return {
    type: actionTypes.CHANNEL_SUCCESS,
    channel,
  };
};

export const channelFail = (error) => {
  return {
    type: actionTypes.CHANNEL_FAIL,
    error,
  };
};

export const channel = (
  title,
  members,
) => {
  return (dispatch) => {
    dispatch(channelStart());
    const channelData = {
      title,
      members,
    };
    const userToken = readFromStorage('token');
    const token = JSON.parse(userToken);
    const url = `${baseUrl}channels`;
    axios({
      method: 'post',
      url,
      headers: { Authorization: `Bearer ${token}` },
      data: channelData,
    }).then((response) => {
      dispatch(channelSuccess(response.data));
    })
      .catch((error) => {
        dispatch(channelFail(getHttpErrorMsg(error)));
      });
  };
};