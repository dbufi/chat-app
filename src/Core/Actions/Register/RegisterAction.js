import axios from 'axios';
import * as actionTypes from '../../ActionTypes';
import baseUrl from '../../../Constants/BaseUrl';

export const registerStart = () => {
  return {
    type: actionTypes.REGISTER_START,
  };
};

export const registerSuccess = (user) => {
  return {
    type: actionTypes.REGISTER_SUCCESS,
    userData: user,
  };
};

export const registerFail = (error) => {
  return {
    type: actionTypes.REGISTER_FAIL,
    error,
  };
};

export const register = (
  name,
  surname,
  email,
  password,
  phone,
  primaryColor,
  secondaryColor,
) => {
  return (dispatch) => {
    dispatch(registerStart());
    const userData = {
      email,
      password,
      firstName: name,
      lastName: surname,
      phoneNumber: phone,
      avatar: {
        primaryColor,
        secondaryColor,
      },


    };
    const url = `${baseUrl}auth/registration`;
    axios
      .post(url, userData)
      .then((response) => {
        dispatch(registerSuccess(response.data));
      })
      .catch((error) => {
        dispatch(registerFail(error.response.data.message));
      });
  };
};
