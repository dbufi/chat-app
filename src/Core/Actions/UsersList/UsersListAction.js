import axios from 'axios';
import * as actionTypes from '../../ActionTypes';
import baseUrl from '../../../Constants/BaseUrl';
import { readFromStorage } from '../../../Constants/StorageManager';
import { getHttpErrorMsg } from '../../../Shared/Utils';

export const usersListStart = () => {
  return {
    type: actionTypes.USERS_LIST_START,
  };
};

export const usersListSuccess = (users) => {
  return {
    type: actionTypes.USERS_LIST_SUCCESS,
    users,
  };
};

export const usersListFail = (error) => {
  return {
    type: actionTypes.USERS_LIST_FAIL,
    error,
  };
};

export const usersList = () => {
  return (dispatch) => {
    dispatch(usersListStart());

    const userToken = readFromStorage('token');
    const token = JSON.parse(userToken);

    const url = `${baseUrl}users`;
    axios({
      method: 'get',
      url,
      headers: { Authorization: `Bearer ${token}` },
    }).then((response) => {
      dispatch(usersListSuccess(response.data));
    })
      .catch((error) => {
        dispatch(usersListFail(getHttpErrorMsg(error)));
      });
  };
};