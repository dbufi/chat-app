import axios from 'axios';
import * as actionTypes from '../../ActionTypes';
import baseUrl from '../../../Constants/BaseUrl';

export const confirmStart = () => {
  return {
    type: actionTypes.CONFIRM_START,
  };
};

export const confirmSuccess = (user) => {
  return {
    type: actionTypes.CONFIRM_SUCCESS,
    userData: user,
  };
};

export const confirmFail = (error) => {
  return {
    type: actionTypes.CONFIRM_FAIL,
    error,
  };
};

export const confirm = (id) => {
  return (dispatch) => {
    dispatch(confirmStart());

    const url = `${baseUrl}auth/confirmation`;

    axios
      .put(url,
        null, {
          params: {
            id,
          },
        })
      .then((response) => {
        dispatch(confirmSuccess(response.data));
      })
      .catch((err) => {
        dispatch(confirmFail(err.response.data));
      });
  };
};
