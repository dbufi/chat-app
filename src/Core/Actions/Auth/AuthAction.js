import axios from 'axios';
import * as actionTypes from '../../ActionTypes';
import baseUrl from '../../../Constants/BaseUrl';
import { saveJsonToStorage, deleteFromStorage } from '../../../Constants/StorageManager';

export const authStart = () => {
  return {
    type: actionTypes.AUTH_START,
  };
};

export const authSuccess = (user, isAuth) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    userData: user,
    userAuth: isAuth,
  };
};

export const authFail = (error) => {
  return {
    type: actionTypes.AUTH_FAIL,
    error,
  };
};

export const logout = () => {
  deleteFromStorage('user');
  deleteFromStorage('token');
  window.location.href = '/';

  return {
    type: actionTypes.USER_LOGOUT,
  };
};


export const login = (email, password) => {
  return (dispatch) => {
    dispatch(authStart());
    const authData = {
      email,
      password,
    };
    const url = `${baseUrl}auth/login`;
    axios
      .post(url, authData)
      .then((response) => {
        saveJsonToStorage('user', response.data);
        saveJsonToStorage('token', response.data.token);
        dispatch(authSuccess(response.data));
      })
      .catch((err) => {
        dispatch(authFail(err.response.data.message));
      });
  };
};