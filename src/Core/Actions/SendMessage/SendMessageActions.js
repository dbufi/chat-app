import axios from 'axios';
import * as actionTypes from '../../ActionTypes';
import baseUrl from '../../../Constants/BaseUrl';
import { readFromStorage } from '../../../Constants/StorageManager';

export const sendMessageStart = () => {
  return {
    type: actionTypes.SEND_MESSAGE_START,
  };
};

export const sendMessageSuccess = (message) => {
  return {
    type: actionTypes.SEND_MESSAGE_SUCCESS,
    newMessage: message,
  };
};

export const sendMessageFail = (error) => {
  return {
    type: actionTypes.SEND_MESSAGE_FAIL,
    error,
  };
};


export const sendMessage = (
  text,
  channelId,
) => {
  return (dispatch) => {
    dispatch(sendMessageStart());
    const messageData = {
      text,
      channelId,
    };
    const userToken = readFromStorage('token');
    const token = JSON.parse(userToken);

    const url = `${baseUrl}messages`;
    axios({
      method: 'post',
      url,
      headers: { Authorization: `Bearer ${token}` },
      data: messageData,
    })
      .catch((error) => {
        dispatch(sendMessageFail(error.response.data));
      });
  };
};
