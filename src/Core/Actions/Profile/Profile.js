import axios from 'axios';
import * as actionTypes from '../../ActionTypes';
import baseUrl from '../../../Constants/BaseUrl';
import { readFromStorage } from '../../../Constants/StorageManager';

export const editProfileStart = () => {
  return {
    type: actionTypes.EDIT_PROFILE_START,
  };
};

export const editProfileSuccess = (user) => {
  return {
    type: actionTypes.EDIT_PROFILE_SUCCESS,
    userData: user,
  };
};

export const editProfileFail = (error) => {
  return {
    type: actionTypes.EDIT_PROFILE_FAIL,
    error,
  };
};

export const editProfile = (
  name,
  surname,
  email,
  password,
  phone,
  primaryColor,
  secondaryColor,
) => {
  return (dispatch) => {
    dispatch(editProfileStart());
    const userData = {
      email,
      password,
      firstName: name,
      lastName: surname,
      phoneNumber: phone,
      avatar: {
        primaryColor,
        secondaryColor,
      },


    };
    const userToken = readFromStorage('token');
    const token = JSON.parse(userToken);

    const url = `${baseUrl}users`;
    axios({
      method: 'patch',
      url,
      headers: { Authorization: `Bearer ${token}` },
      data: userData,
    }).then((response) => {
      localStorage.setItem('user', JSON.stringify(response.data));
      dispatch(editProfileSuccess(response.data));
    })
      .catch((error) => {
        dispatch(editProfileFail(error.response.data));
      });
  };
};