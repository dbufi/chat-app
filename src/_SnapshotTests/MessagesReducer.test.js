import reducer from '../Core/Reducers/MessagesReducer';
import * as actionTypes from '../Core/ActionTypes';

describe('Messages reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      channelId: '',
      messages: [],
      error: null,
      loading: false,
    });
  });

  it('should get all messages ', () => {
    const messages = {
      _id: 'string',
      text: 'string',
      user: {
        _id: 'string',
        firstName: 'string',
        lastName: 'string',
        email: 'user@example.com',
        password: 'string',
        active: true,
        confirmed: true,
        avatar: {
          primaryColor: 'string',
          secondaryColor: 'string',
        },
        createdAt: '2020-04-09T10:55:39.887Z',
        updatedAt: '2020-04-09T10:55:39.887Z',
        deletedAt: '2020-04-09T10:55:39.887Z',
      },
      channel: {
        _id: 'string',
        title: 'string',
        lastMessage: 'string',
        adminId: 'string',
        members: [
          'string',
        ],
        createdAt: '2020-04-09T10:55:39.887Z',
        updatedAt: '2020-04-09T10:55:39.887Z',
        deletedAt: '2020-04-09T10:55:39.887Z',
      },
      createdAt: '2020-04-09T10:55:39.887Z',
      updatedAt: '2020-04-09T10:55:39.887Z',
      deletedAt: '2020-04-09T10:55:39.887Z',
    };

    expect(reducer({
      channelId: '',
      messages: [],
      error: null,
      loading: false,
    }, {
      type: actionTypes.MESSAGES_SUCCESS,
      messages,
    })).toEqual({
      messages,
      error: null,
      loading: false,
    });
  });
});
