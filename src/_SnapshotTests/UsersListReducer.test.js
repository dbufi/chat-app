import reducer from '../Core/Reducers/UsersListReducer';
import * as actionTypes from '../Core/ActionTypes';

describe('users list reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      users: [],
      error: null,
      loading: false,
    });
  });

  it('should get the users ', () => {
    const users = {
      _id: 'string',
      firstName: 'string',
      lastName: 'string',
      email: 'user@example.com',
      password: 'string',
      active: true,
      confirmed: true,
    };

    expect(reducer({
      users: [],
      error: null,
      loading: false,
    }, {
      type: actionTypes.USERS_LIST_SUCCESS,
      users,
    })).toEqual({
      users,
      error: null,
      loading: false,
    });
  });
});
