import reducer from '../Core/Reducers/ChannelsListReducer';
import * as actionTypes from '../Core/ActionTypes';

describe('Channels list reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      channels: [],
      error: null,
      loading: false,
    });
  });

  it('should get all channels ', () => {
    const channels = [
      {
        _id: 'string',
        title: 'string',
        lastMessage: {
          _id: 'string',
          text: 'string',
          user: 'string',
          channel: 'string',
          createdAt: '2020-04-09T11:09:01.406Z',
          updatedAt: '2020-04-09T11:09:01.406Z',
          deletedAt: '2020-04-09T11:09:01.406Z',
        },
        adminId: 'string',
        members: [
          {
            _id: 'string',
            firstName: 'string',
            lastName: 'string',
            email: 'user@example.com',
            password: 'string',
            active: true,
            confirmed: true,
            avatar: {
              primaryColor: 'string',
              secondaryColor: 'string',
            },
            createdAt: '2020-04-09T11:09:01.406Z',
            updatedAt: '2020-04-09T11:09:01.406Z',
            deletedAt: '2020-04-09T11:09:01.406Z',
          },
        ],
        createdAt: '2020-04-09T11:09:01.406Z',
        updatedAt: '2020-04-09T11:09:01.406Z',
        deletedAt: '2020-04-09T11:09:01.406Z',
      },
    ];

    expect(reducer({
      channels: [],
      error: null,
      loading: false,
    }, {
      type: actionTypes.CHANNELS_LIST_SUCCESS,
      channels,
    })).toEqual({
      channels,
      error: null,
      loading: false,
    });
  });
});
