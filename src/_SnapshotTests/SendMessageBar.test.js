import React from 'react';
import renderer from 'react-test-renderer';
import {
  BrowserRouter,
} from 'react-router-dom';
import { Provider } from 'react-redux';
import SendMessageBar from '../Components/Screens/Messages/SendMessageBar';
import routes from '../Constants/Routes';
import Routes from '../Components/Common/Routes/PublicRoutes';
import { store } from '../Core/Store/Store';

describe('SendMessageBar component', () => {
  it('should render Component', () => {
    const wrapper = renderer.create(<Provider store={store}>
      <BrowserRouter>
        <Routes path={routes.App.Chat}>
          <SendMessageBar />
        </Routes>
      </BrowserRouter>
    </Provider>);
    expect(wrapper).toMatchSnapshot();
  });
});