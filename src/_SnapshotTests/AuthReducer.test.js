import reducer from '../Core/Reducers/AuthReducer';
import * as actionTypes from '../Core/ActionTypes';

describe('auth reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      user: {},
      error: null,
      loading: false,
      isAuth: false,
    });
  });

  it('should store the token upon login', () => {
    const user = {
      _id: 'string',
      firstName: 'string',
      lastName: 'string',
      email: 'user@example.com',
    };

    expect(reducer({
      user: {},
      error: null,
      loading: false,
      isAuth: false,
    }, {
      type: actionTypes.AUTH_SUCCESS,
      userData: user,
    })).toEqual({
      user,
      error: null,
      loading: false,
      isAuth: true,
    });
  });
});
