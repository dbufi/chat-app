import React from 'react';
import renderer from 'react-test-renderer';
import {
  BrowserRouter,
} from 'react-router-dom';
import { Provider } from 'react-redux';
import EmailVerification from '../Components/Screens/ResetPassword/EmailVerification';
import routes from '../Constants/Routes';
import Routes from '../Components/Common/Routes/PublicRoutes';
import { store } from '../Core/Store/Store';


describe('Email component', () => {
  it('should render Component', () => {
    const wrapper = renderer.create(<Provider store={store}>
      <BrowserRouter>
        <Routes path={routes.Auth.Login}>
          <EmailVerification />
        </Routes>
      </BrowserRouter>
    </Provider>);
    expect(wrapper).toBeTruthy();
  });
});