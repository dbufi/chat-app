import React from 'react';
import { Provider } from 'react-redux';
import {
  BrowserRouter,
} from 'react-router-dom';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import routes from '../Constants/Routes';
import Routes from '../Components/Common/Routes/PublicRoutes';
import Login from '../Components/Screens/Login/Presentantional';
import { store } from '../Core/Store/Store';

describe('Login component', () => {
  Enzyme.configure({ adapter: new Adapter() });
  it('should render snapshot', () => {
    const wrapper = shallow(<Provider store={store}>
      <BrowserRouter>
        <Routes path={routes.Auth.Login}>
          <Login />
        </Routes>
      </BrowserRouter>
    </Provider>).dive();
    expect(wrapper).toBeTruthy();
  });
});