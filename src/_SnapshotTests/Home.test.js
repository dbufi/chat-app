import React from 'react';
import renderer from 'react-test-renderer';
import {
  BrowserRouter,
} from 'react-router-dom';
import { Provider } from 'react-redux';
import Home from '../Components/Screens/Home/Presentantional';
import routes from '../Constants/Routes';
import Routes from '../Components/Common/Routes/PublicRoutes';
import { store } from '../Core/Store/Store';

describe('Home component', () => {
  it('should render Component', () => {
    const wrapper = renderer.create(<Provider store={store}>
      <BrowserRouter>
        <Routes path={routes.App.Chat}>
          <Home />
        </Routes>
      </BrowserRouter>
    </Provider>);
    expect(wrapper).toBeTruthy();
  });
});