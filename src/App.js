import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './App.css';
import {
  BrowserRouter,
  Switch,
  Redirect,
  Route,
} from 'react-router-dom';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { authSuccess } from './Core/Actions/Auth/AuthAction';
import Register from './Components/Screens/Register/Presentantional';
import ConfirmAcount from './Components/Screens/Register/ConfirmAcount';
import PrivateRoutes from './Components/Common/Routes/PrivateRoutes';
import PublicRoutes from './Components/Common/Routes/PublicRoutes';
import routes from './Constants/Routes';
import Login from './Components/Screens/Login/Presentantional';
import EmailVerification from './Components/Screens/ResetPassword/EmailVerification';
import { readFromStorage } from './Constants/StorageManager';
import NewPassword from './Components/Screens/ResetPassword/NewPassword';
import EditProfile from './Components/Screens/Profile/Profile';
import Home from './Components/Screens/Home/Presentantional';
import { initSocket } from './Socket';
import NotFound from './Components/Screens/404/Presentantional';

function App() {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const user = JSON.parse(readFromStorage('user'));
  if (user !== null) {
    dispatch(authSuccess(user, true));
  }

  const channels = useSelector(state => state.channels.channels);


  initSocket(dispatch, channels, user, t);

  return (
    <BrowserRouter>
      <Switch>
        <Route
          exact
          path="/"
          render={() => <Redirect to={routes.Auth.Login} />}
        />
        <PublicRoutes path={routes.Auth.Register}>
          <Register />
        </PublicRoutes>
        <PublicRoutes path={routes.Auth.Confirm}>
          <ConfirmAcount />
        </PublicRoutes>
        <PublicRoutes path={routes.Auth.Login}>
          <Login />
        </PublicRoutes>
        <PublicRoutes path={routes.Auth.Email}>
          <EmailVerification />
        </PublicRoutes>
        <PublicRoutes path={routes.Auth.Password}>
          <NewPassword />
        </PublicRoutes>
        <PrivateRoutes path={routes.App.Profile}>
          <EditProfile />
        </PrivateRoutes>
        <PrivateRoutes path={routes.App.Chat}>
          <Home />
        </PrivateRoutes>
        <Route path="*">
          <NotFound />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

App.propTypes = {
  isAuth: PropTypes.bool,
  store: PropTypes.object,
};

export default App;
