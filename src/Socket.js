import React from 'react';
import socketIOClient from 'socket.io-client';
import { notification } from 'antd';
import UIfx from 'uifx';
import { MessageTwoTone } from '@ant-design/icons';
import EllipsisText from 'react-ellipsis-text';
import baseUrl from './Constants/BaseUrl';
import { sendMessageSuccess } from './Core/Actions/SendMessage/SendMessageActions';
import { channelList } from './Core/Actions/ChannelsList/ChannelsListAction';
import sms from './Assets/sms.mp3';

const socket = socketIOClient(`${baseUrl}`);

export const initSocket = (dispatch, channel, user, t) => {
  socket.off();
  socket.on('MESSAGE-CREATED', (data) => {
    data.user = { _id: data.user };
    data.channel = { _id: data.channel };

    dispatch(sendMessageSuccess(data));
    if (channel.filter(c => c._id.includes(data.channel._id)).length !== 0 && data.user._id !== user._id) {
      notification.open({ message: t('messages.message'), description: <EllipsisText text={data.text} length={19} />, icon: <MessageTwoTone /> });
      const beep = new UIfx(sms);
      beep.play();
    }
  });

  socket.on('CHANNEL-CREATED', () => {
    dispatch(channelList());
  });

  socket.on('CHANNEL-UPDATED', () => {
    return dispatch(channelList());
  });
};