export const color = {
  white: '#fff',
  blue: '#1675d1',
  darkBlue: '#07075a',
  grey: '#e0e0e0',
  whiteBlue: '#e6e6fa',
  whiteGrey: '#e5e5e5',
  mainColor: '#0084ff',
  message: '#b0bec5',
};