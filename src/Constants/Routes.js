const APP_INDEX = '/chat';
const AUTH_INDEX = '/auth';

const routes = {
  App: {
    Chat: `${APP_INDEX}/:id`,
    Profile: `${APP_INDEX}/profile`,
  },
  Auth: {
    Register: `${AUTH_INDEX}/register`,
    Confirm: `${AUTH_INDEX}/confirm/:id`,
    Login: `${AUTH_INDEX}/login`,
    Email: `${AUTH_INDEX}/email`,
    Password: `${AUTH_INDEX}/reset_password/:id`,
  },
};

export default routes;