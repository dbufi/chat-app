import * as Yup from 'yup';

export const RegisterSchema = (t) => {
  return Yup.object().shape({
    name: Yup.string()
      .min(2, t('error.short'))
      .max(50, t('error.long'))
      .required(t('error.required')),
    surname: Yup.string()
      .min(2, t('error.short'))
      .max(50, t('error.long'))
      .required(t('error.required')),
    email: Yup.string()
      .email(t('error.email'))
      .required(t('error.required')),
    phone: Yup.string()
      .required(t('error.required')),
    password: Yup.string()
      .min(2, t('error.short'))
      .required(t('error.required')),
  });
};

export const LoginSchema = (t) => {
  return Yup.object().shape({
    email: Yup.string()
      .email(t('error.email'))
      .required(t('error.required')),
    password: Yup.string()
      .min(2, t('error.short'))
      .required(t('error.required')),
  });
};

export const EmailValidation = (t) => {
  return Yup.object().shape({
    email: Yup.string()
      .email(t('error.email'))
      .required(t('error.required')),
  });
};