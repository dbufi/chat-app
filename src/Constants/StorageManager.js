export const readFromStorage = (data) => { return localStorage.getItem(data); };

export const saveJsonToStorage = (name, data) => { return localStorage.setItem(name, JSON.stringify(data)); };

export const saveToStorage = (name, data) => { return localStorage.setItem(name, data); };

export const deleteFromStorage = (data) => { return localStorage.removeItem(data); };
