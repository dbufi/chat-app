export const updateObject = (oldObject, updatedProperties) => {
  return {
    ...oldObject,
    ...updatedProperties,
  };
};

export const getHttpErrorMsg = (err) => {
  let message = '';
  if (err.response !== undefined) {
    try {
      message = err.response.data;
    } catch (e) {
      message = 'Something went wrong.';
    }
  } else {
    message = err.message;
  }

  return message;
};