import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Provider } from 'react-redux';
import App from './App';
import * as serviceWorker from './serviceWorker';
import './Locales/index';

import { store } from './Core/Store/Store';


const app = (
  <Provider store={store}>
     <App store={store} />
  </Provider>
);
ReactDOM.render(app,
  document.getElementById('root'));


serviceWorker.unregister();
