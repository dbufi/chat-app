import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import en from './en.json';
import al from './al.json';
import storageKeys from '../Constants/Storage';

const lng = localStorage.getItem(storageKeys.LANGUAGE) || 'en';

i18n.use(initReactI18next).init({
  interpolation: { escapeValue: false },
  lng,
  resources: {
    en,
    al,
  },
});

export default i18n;
