import styled from 'styled-components';
import { color } from '../../../Constants/Colors';

export const SideBar = styled.div`
  background-color: ${color.blue};
  width: 431px;
  margin-left: 0px;
  height: 100vh;
  display: flex;
  align-items: center;
`;
