import styled from 'styled-components';
import { color } from '../../../Constants/Colors';

export const Content = styled.div`
  background-color: ${color.whiteGrey};
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: flex-end;
`;
