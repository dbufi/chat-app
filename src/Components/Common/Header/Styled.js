import styled from 'styled-components';
import { color } from '../../../Constants/Colors';

export const Header = styled.div`
  background-color: ${color.mainColor};
  margin-top: 0px;
  height: 80px;
  display: flex;
  justify-content: space-between;
  align-items:center;
  position: sticky;
  top: 0;
  z-index: 100%;
`;
