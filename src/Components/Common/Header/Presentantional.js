import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  Avatar, Button, Modal, Popover,
} from 'antd';

import { UsergroupAddOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import { Header } from './Styled';
import CreateChannel from '../../Screens/Channel/Presentantional';
import i18n from '../../../Locales/index';
import { logout } from '../../../Core/Actions/Auth/AuthAction';
import storageKeys from '../../../Constants/Storage';
import { readFromStorage, saveToStorage } from '../../../Constants/StorageManager';


const AppHeader = () => {
  const user = useSelector(state => state.auth.user);
  const [visible, setVisible] = useState(false);
  const primaryColor = user.avatar.primaryColor;
  const secondaryColor = user.avatar.secondaryColor;

  const dispatch = useDispatch();

  const handlelogout = () => {
    dispatch(logout());
  };

  const { t } = useTranslation();

  const setLanguage = (lng) => {
    saveToStorage(storageKeys.LANGUAGE, lng);
    i18n.changeLanguage(lng);
  };

  const content = (
    <div>
      <Link to="profile"><p style={{ cursor: 'context-menu', color: 'black' }}>{t('header.profile')}</p> </Link>
      <p onClick={() => setLanguage('en')} style={{ cursor: 'context-menu' }}> {readFromStorage('lng') === 'en' ? (<strong> EN </strong>) : ('EN')} </p>
      <p onClick={() => setLanguage('al')} style={{ cursor: 'context-menu' }}>{readFromStorage('lng') === 'al' ? (<strong> AL </strong>) : ('AL')} </p>
      <p onClick={handlelogout} style={{ cursor: 'context-menu' }}>{t('header.logout')}</p>
    </div>
  );

  const name = user.firstName;
  const surname = user.lastName;
  const firstLetter = name.charAt(0);
  const secondLetter = surname.charAt(0);
  const userInitials = firstLetter + secondLetter;

  const closeModal = () => {
    setVisible(false);
  };

  const handleModal = () => {
    setVisible(true);
  };


  return (
    <Header>
     <Link to="home"> <h1 style={{ color: 'white', marginLeft: '20px' }}>Softup Chat</h1> </Link>
      <Button type="default" shape="circle" icon={<UsergroupAddOutlined />} onClick={handleModal} size="large"/>
      <Popover placement="bottom" content={content} trigger="click">
        <Avatar
        style={{
          backgroundColor: primaryColor,
          verticalAlign: 'middle',
          color: secondaryColor,
          marginRight: '20px',
        }}
        size="large"
      >
        {userInitials}
      </Avatar>
      </Popover>
      <Modal
          title={t('channel.new')}
          visible={visible}
          footer={false}
          onCancel={closeModal}
        >
          <CreateChannel closeModal={closeModal} channelsMembers={null}/>
        </Modal>
    </Header>
  );
};

export default AppHeader;