import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import routes from '../../../Constants/Routes';

const PublicRoutes = (props) => {
  const isAuth = useSelector(state => state.auth.isAuth);

  return (isAuth ? <Redirect to={routes.App.Chat}/> : <Route {...props}>{props.children}</Route>);
};

PublicRoutes.propTypes = {
  isAuth: PropTypes.bool,
  children: PropTypes.element,
};

export default PublicRoutes;