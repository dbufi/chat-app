import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import routes from '../../../Constants/Routes';

const PrivateRoutes = (props) => {
  const isAuth = useSelector(state => state.auth.isAuth);

  return (!isAuth ? <Redirect to={routes.Auth.Login}/> : <Route {...props}>{props.children}</Route>);
};

PrivateRoutes.propTypes = {
  isAuth: PropTypes.bool,
  children: PropTypes.element,
};

export default PrivateRoutes;