import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import UsersList from '../UsersList/UsersList';
import { editChannel } from '../../../Core/Actions/EditChannel/EditChannel';
import { StyledInput, Text, Wrapper } from '../Profile/Styles';
import { SaveButton } from '../Styled';

const EditChannel = ({
  closeModal, selectedChannelId, channelsMembers, selectedTitle,
}) => {
  const user = useSelector(state => state.auth.user);
  const [selectedMemberIds, setMemberIds] = useState([user._id]);
  const [selectedChannelTitle, setSelectedChannelTitle] = useState(selectedTitle);
  const dispatch = useDispatch();
  const loading = useSelector(state => state.editChannel.loading);
  const { t } = useTranslation();

  const setUserIds = (value) => {
    setMemberIds(value);
  };

  const handleTitle = (event) => {
    setSelectedChannelTitle(event.target.value);
  };

  const setChannelData = () => {
    dispatch(editChannel(selectedChannelId, selectedChannelTitle, [...selectedMemberIds, user._id]));
    closeModal();
  };


  return (
    <div>
      <Wrapper>
        <Text>{t('channel.name')}</Text>
        <StyledInput onChange={handleTitle} value={selectedChannelTitle} />
      </Wrapper>
      <Wrapper>
        <Text>{t('channel.members')}</Text>
        <UsersList setUserIds={setUserIds} channelsMembers={channelsMembers} channelId={selectedChannelId}/>
      </Wrapper>
      <SaveButton loading={ loading } type="primary" onClick={setChannelData}>{t('channel.save')}</SaveButton>
    </div>
  );
};

EditChannel.propTypes = {
  closeModal: PropTypes.func,
  selectedChannelId: PropTypes.string,
  channelsMembers: PropTypes.array,
  selectedTitle: PropTypes.string,
};


export default EditChannel;