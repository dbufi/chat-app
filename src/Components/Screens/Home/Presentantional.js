import React from 'react';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import ChannelsList from '../ChannelsList/Presentantional';
import Header from '../../Common/Header/Presentantional';
import SendMessageBar from '../Messages/SendMessageBar';
import Messages from '../Messages/Presentantional';
import ChannelTitle from '../Messages/Title';
import { Wrapper, ComponentsWrapper } from './Styles';

const Home = () => {
  const { id } = useParams();
  const channels = useSelector(state => state.channels.channels);
  const validChannelId = channels.filter(c => c._id === id);

  return (
    <div>
      <Header />
      <ComponentsWrapper>
        <ChannelsList />
        {validChannelId.length !== 0 ? <Wrapper>
          <ChannelTitle />
          <Messages />
          <SendMessageBar />
        </Wrapper> : null }
      </ComponentsWrapper>
    </div>
  );
};

export default Home;