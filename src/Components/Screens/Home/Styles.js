import styled from 'styled-components';
import { device } from '../../../Constants/Devices';


export const Wrapper = styled.div`
  display: flex; 
  flex-direction: column; 
  height: 100%; 
  width: 80%;
  margin-left: 357px;

  @media ${device.mobileL} { 
    height: 96%;
    margin-left: 189px
  };
`;

export const ComponentsWrapper = styled.div`
  display: flex; 
  flexDirection: row; 
  height: 90vh;
`;