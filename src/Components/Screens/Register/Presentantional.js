import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Formik } from 'formik';
import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { notification, Result, Button } from 'antd';
import PropTypes from 'prop-types';
import { RegisterSchema } from '../../../Constants/Validations';
import {
  FormInput, SubmitButton, Form, FormContent, Text, Logo, Image, Wrapper, Error, ColorPicker, FormWrapper,
} from '../Styled';
import 'antd/dist/antd.css';
import logo from '../../../Assets/Images/logo.png';
import { color } from '../../../Constants/Colors';
import { register } from '../../../Core/Actions/Register/RegisterAction';

const Register = () => {
  const [primaryColor, setPrimaryColor] = useState(color.white);
  const [secondaryColor, setSecondaryColor] = useState(color.white);
  const error = useSelector(state => state.register.error);
  const loading = useSelector(state => state.register.loading);
  const success = useSelector(state => state.register.success);
  const dispatch = useDispatch();

  useEffect(() => {
    if (error !== null) {
      notification.error({ message: error });
    }
  }, [error]);

  const handlePrimaryColor = (colors) => {
    setPrimaryColor(colors.hex);
  };

  const handleSecondaryColor = (colors) => {
    setSecondaryColor(colors.hex);
  };

  const { t } = useTranslation();

  return (

    <Formik
      initialValues={{
        name: '', surname: '', email: '', password: '', phone: '',
      }}
      validationSchema={RegisterSchema(t)}
      onSubmit={(values) => {
        dispatch(register(values.name, values.surname, values.email, values.password, values.phone, primaryColor, secondaryColor));
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleSubmit,
      }) => (
      <FormWrapper>
        <FormContent>
          <Form>
            <Logo>
              <Image src={logo} alt='' height="200" widht="200" />
            </Logo>
            {!success ? <Wrapper>
              <h1 style={{ marginLeft: '85px', marginTop: '10px', color: color.blue }}>Rregjistrimi</h1>
              <Text>
                {t('register.name')}
              </Text>
              <FormInput
                type="text"
                name="name"
                placeholder={t('register.name')}
                onChange={handleChange}
                value={values.name}
                className={
                  errors.name && touched.name
                    ? 'text-input error'
                    : 'text-input'
                }
              />
              <Error>
                {errors.name && touched.name && (
                  <div className="input-feedback">{errors.name}</div>
                )}
              </Error>
              <Text>
                {t('register.surname')}
              </Text>
              <FormInput
                type="text"
                name="surname"
                placeholder={t('register.surname')}
                onChange={handleChange}
                value={values.surname}
              />
              <Error>
                {errors.surname && touched.surname && (
                  <div className="input-feedback">{errors.surname}</div>
                )}
              </Error>
              <Text>
                {t('register.email')}
              </Text>
              <FormInput
                type="email"
                name="email"
                placeholder={t('register.email')}
                onChange={handleChange}
                value={values.email}
              />
              <Error>
                {errors.email && touched.email && (
                  <div className="input-feedback">{errors.email}</div>
                )}
              </Error>
              <Text>
                {t('register.password')}
              </Text>
              <FormInput
                type="password"
                name="password"
                placeholder={t('register.password')}
                onChange={handleChange}
                value={values.password}
              />
              <Error>
                {errors.password && touched.password && (
                  <div className="input-feedback">{errors.password}</div>
                )}
              </Error>
              <Text>
                {t('register.phone')}
              </Text>
              <FormInput
                type="text"
                name="phone"
                placeholder={t('register.phone')}
                onChange={handleChange}
                value={values.phone}
              />
              <Error>
                {errors.phone && touched.phone && (
                  <div className="input-feedback">{errors.phone}</div>
                )}
              </Error>
              <Text>
                {t('register.primary')}
              </Text>
              <ColorPicker
                color={primaryColor}
                onChangeComplete={handlePrimaryColor} />
              <Text>
                {t('register.secondary')}
              </Text>
              <ColorPicker
                color={secondaryColor}
                onChangeComplete={handleSecondaryColor} />
              <SubmitButton
                type="primary"
                shape="round"
                size="medium"
                loading={loading}
                onClick={handleSubmit}>
                {t('register.account')}
              </SubmitButton>
              <Link to="/auth/login">
                <Text>{t('register.loginText')}</Text>
              </Link>
            </Wrapper>
              : <Result
                status="success"
                title={t('register.successRegister')}
                extra={[
                  <Button type="primary" key="console">
                    <Link to='/auth/login'>
                      {t('register.loginText')}
                    </Link>
                  </Button>,
                ]}
              />
            }
          </Form>
        </FormContent>
      </FormWrapper>
      )}
    </Formik>
  );
};

Register.propTypes = {
  register: PropTypes.func,
  success: PropTypes.bool,
  error: PropTypes.string,
  loading: PropTypes.bool,
};


export default Register;