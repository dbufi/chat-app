import React, { useEffect } from 'react';
import {
  useParams,
  Link,
} from 'react-router-dom';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import {
  ConfirmCard, MailImage, Message, ConfirmWrapper,
} from '../Styled';
import mail from '../../../Assets/Images/mail.png';
import { confirm } from '../../../Core/Actions/ConfirmAcount/ConfirmActions';


const ConfirmAcount = () => {
  const { t } = useTranslation();
  const { id } = useParams();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(confirm(id));
  }, [dispatch, id]);

  return (
    <div style={{ backgroundColor: 'wheat', height: '100vh' }}>
      <ConfirmWrapper>
        <ConfirmCard>
          <MailImage src={mail} alt='' />
          <Message>
            {t('register.confirmMessage')}
          </Message>
          <Message>
            <Link to="/auth/login">
              {t('register.loginText')}
            </Link>
          </Message>
        </ConfirmCard>
      </ConfirmWrapper>
    </div>
  );
};

ConfirmAcount.propTypes = {
  confirm: PropTypes.func,
};

export default ConfirmAcount;
