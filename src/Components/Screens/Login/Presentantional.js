import React, { useEffect } from 'react';
import { Button, notification } from 'antd';
import { Formik } from 'formik';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import {
  Error,
  LoginText,
  StyledInput,
  Image,
} from '../Styled';
import { SideBar } from '../../Common/Sidebar/Styles';
import { Content } from '../../Common/Content/Styles';
import { ContentWrapper } from '../../Common/Wrapper/ContentWrapper';
import { Wrapper } from '../../../Styles/Styles';
import 'antd/dist/antd.css';
import { LoginSchema } from '../../../Constants/Validations';
import { login } from '../../../Core/Actions/Auth/AuthAction';
import chat from '../../../Assets/Images/chat.jpg';
import logo from '../../../Assets/Images/logo.png';
import { color } from '../../../Constants/Colors';

const Login = () => {
  const loading = useSelector(state => state.auth.loading);
  const error = useSelector(state => state.auth.error);
  const dispatch = useDispatch();

  useEffect(() => {
    if (error !== null) {
      notification.error({ message: error });
    }
  }, [error]);

  const { t } = useTranslation();

  return (
    <div>
      <Formik
        initialValues={{ email: '', password: '' }}
        onSubmit={(values) => {
          dispatch(login(values.email, values.password));
        }}
        validationSchema={LoginSchema(t)}
      >
        {(formProps) => {
          const {
            values,
            touched,
            errors,
            handleChange,
            handleSubmit,
          } = formProps;

          return (
            <div>
              <ContentWrapper>
                <SideBar>
                  <Wrapper>
                    <Image>
                      <img src={logo} style={{ marginLeft: '13px' }} alt="" />
                    </Image>
                    <LoginText>{t('register.email')}</LoginText>
                    <StyledInput
                      id="email"
                      type="text"
                      value={values.email}
                      onChange={handleChange}
                      className={
                        errors.email && touched.email
                          ? 'text-input error'
                          : 'text-input'
                      }
                    />
                    <Error>
                      {errors.email && touched.email && (
                        <div className="input-feedback">{errors.email}</div>
                      )}
                    </Error>
                    <LoginText>{t('register.password')}</LoginText>
                    <StyledInput
                      id="password"
                      type="password"
                      value={values.password}
                      onChange={handleChange}
                      onPressEnter={handleSubmit}
                    />
                    <Error>
                      {errors.password && touched.password && (
                        <div className="input-feedback">{errors.password}</div>
                      )}
                    </Error>
                    <Button type="primary" shape="round" loading={loading} onClick={handleSubmit} style={{ marginTop: '10px', width: '-webkit-fill-available' }}>
                      {t('register.login')}
                    </Button>
                    <Button type="primary" shape="round" style={{
                      marginTop: '10px', width: '-webkit-fill-available', background: 'white', color: 'blue',
                    }}>
                      <Link to="/auth/register" style={{ color: color.blue }}>
                        {t('register.account')}
                      </Link>
                    </Button>
                    <Link to="/auth/email" >
                      <p style={{ marginLeft: '10px', marginTop: '10px', color: 'black' }}>{t('register.resetPassword')}</p>
                    </Link>
                  </Wrapper>
                </SideBar>
                <Content>
                  <img
                    src={chat}
                    style={{ width: '100%', height: '100%' }}
                    alt=""
                  />
                </Content>
              </ContentWrapper>
            </div>
          );
        }}
      </Formik>
    </div>
  );
};

export default Login;
