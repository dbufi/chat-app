import styled from 'styled-components';
import { Input, Card, Button } from 'antd';

export const Text = styled.p`
  margin-left: 10px;
`;

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const StyledInput = styled(Input)`
  width: 60%;
  margin-bottom:10px;
  position: static;
`;

export const StyledCard = styled(Card)`
  width: 95%;
  margin-top: 10px;
  margin-bottom: 10px;
  box-shadow: 5px 5px 20px darkgrey;
  position: static;
`;

export const ProfileWrapper = styled.div`
  display: flex; 
  align-items: center; 
  flex-direction: column;
`;

export const H3 = styled.h3`
  margin-left: 10px;
`;

export const SaveButton = styled(Button)`
  margin-top: 10px;
  margin-bottom: 20px;
  width: 20%;
`;

export const Error = styled.h4`
  color: red;
`;

export const EmailInput = styled(Input)`
  width: 100%;
  margin-bottom:10px;
  position: static;
`;

export const EmailWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 60%;
`;

export const H1 = styled.h1`
  margin-top: 10px;
`;