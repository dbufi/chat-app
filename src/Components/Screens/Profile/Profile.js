import React, { useState } from 'react';
import 'antd/dist/antd.css';
import { BlockPicker } from 'react-color';
import { useDispatch, useSelector } from 'react-redux';
import { Formik } from 'formik';
import { useTranslation } from 'react-i18next';
import {
  Text, Wrapper, StyledInput, StyledCard, ProfileWrapper, H3, SaveButton, Error, EmailInput, EmailWrapper, H1,
} from './Styles';
import AppHeader from '../../Common/Header/Presentantional';
import { editProfile } from '../../../Core/Actions/Profile/Profile';
import { EmailValidation } from '../../../Constants/Validations';


const EditProfile = () => {
  const user = useSelector(state => state.auth.user);
  const [primaryColor, setPrimaryColor] = useState(user.avatar.primaryColor);
  const [secondaryColor, setSecondaryColor] = useState(user.avatar.secondaryColor);

  const dispatch = useDispatch();
  const { t } = useTranslation();

  const loading = useSelector(state => state.auth.loading);

  const handlePrimaryColor = (color) => {
    setPrimaryColor(color.hex);
  };

  const handleSecondaryColor = (color) => {
    setSecondaryColor(color.hex);
  };

  return (
    <div>
      <Formik
        initialValues={{
          email: user.email, name: user.firstName, surname: user.lastName, phone: user.phoneNumber, password: user.password,
        }}
        onSubmit={(values) => {
          dispatch(editProfile(values.name, values.surname, values.email, values.password, values.phone, primaryColor, secondaryColor));
        }}
        validationSchema={EmailValidation(t)}
      >
        {(formProps) => {
          const {
            values,
            errors,
            touched,
            handleChange,
            handleSubmit,
          } = formProps;

          return (
            <div>
              <AppHeader />
              <ProfileWrapper>
                <H1> <strong> {t('profile.update')} </strong> </H1>
                <StyledCard title={t('profile.credentials')} >
                  <Wrapper>
                    <Text> {t('register.email')} </Text>
                    <EmailWrapper>
                      <EmailInput
                        name="email"
                        type="text"
                        value={values.email}
                        onChange={handleChange}
                        className={
                          errors.email && touched.email
                            ? 'text-input error'
                            : 'text-input'
                        }
                      />
                      <Error>
                        {errors.email && touched.email && (
                          <div className="input-feedback">{errors.email}</div>
                        )}
                      </Error>
                    </EmailWrapper>
                  </Wrapper>
                  <Wrapper>
                    <Text>
                      {t('register.password')}
                    </Text>
                    <StyledInput
                      name="password"
                      type="password"
                      value={values.password}
                      onChange={handleChange}
                    />
                  </Wrapper>
                </StyledCard>
                <StyledCard title={t('profile.info')}>
                  <Wrapper>
                    <Text>
                      {t('register.name')}
                    </Text>
                    <StyledInput
                      name="name"
                      type="text"
                      value={values.name}
                      onChange={handleChange} />
                  </Wrapper>
                  <Wrapper>
                    <Text>
                      {t('register.surname')}
                    </Text>
                    <StyledInput
                      name="surname"
                      type="text"
                      value={values.surname}
                      onChange={handleChange} />
                  </Wrapper>
                  <Wrapper>
                    <Text>
                      {t('register.phone')}
                    </Text>
                    <StyledInput
                      name="phone"
                      type="text"
                      value={values.phone}
                      onChange={handleChange} />
                  </Wrapper>
                </StyledCard >
                <StyledCard title={t('profile.config')} >
                  <Wrapper>
                    <ProfileWrapper>
                      <H3>
                        {t('register.primary')}
                      </H3>
                      <BlockPicker
                        color={primaryColor}
                        onChangeComplete={handlePrimaryColor}
                      />
                    </ProfileWrapper>
                    <ProfileWrapper>
                      <H3>
                        {t('register.secondary')}
                      </H3>
                      <BlockPicker
                        color={secondaryColor}
                        onChangeComplete={handleSecondaryColor}
                      />
                    </ProfileWrapper>
                  </Wrapper>
                </StyledCard>
                <SaveButton type="primary" size="large" loading={loading} onClick={handleSubmit} >
                  {t('register.save')}
                </SaveButton>
              </ProfileWrapper>
            </div>
          );
        }}
      </Formik>
    </div>
  );
};

export default EditProfile;
