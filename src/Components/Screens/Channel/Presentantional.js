import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import UsersList from '../UsersList/UsersList';
import { channel } from '../../../Core/Actions/Channel/ChannelAction';
import { StyledInput, Text, Wrapper } from '../Profile/Styles';
import { SaveButton } from '../Styled';

const CreateChannel = ({ closeModal }) => {
  const user = useSelector(state => state.auth.user);
  const [members, setMembers] = useState(user._id);
  const [title, setTitle] = useState('');
  const dispatch = useDispatch();
  const loading = useSelector(state => state.channel.loading);

  const { t } = useTranslation();
  const setUserIds = (value) => {
    setMembers(value);
  };

  const handleTitle = (event) => {
    setTitle(event.target.value);
  };

  const setChannelData = () => {
    dispatch(channel(title, [...members, user._id]));
    closeModal();
  };

  return (
    <div>
      <Wrapper>
        <Text>{t('channel.name')}</Text>
        <StyledInput onChange={handleTitle} placeholder={t('channel.name')} />
      </Wrapper>
      <Wrapper>
        <Text>{t('channel.members')}</Text>
        <UsersList setUserIds={setUserIds} channelsMembers={[]} channelId={''} />
      </Wrapper>
      <SaveButton loading={ loading } type="primary" onClick={setChannelData}>{t('channel.save')}</SaveButton>
    </div>
  );
};

CreateChannel.propTypes = {
  closeModal: PropTypes.func,
};

export default CreateChannel;