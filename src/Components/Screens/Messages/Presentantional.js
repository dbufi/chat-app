import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { Avatar, Popover } from 'antd';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import {
  Wrapper, Message, MyMessage, Text, MyText,
} from './Styles';
import { messages } from '../../../Core/Actions/Messages/MessagesAction';
import { dateTimeFormater } from '../../../Constants/DateTimeFormat';

const Messages = () => {
  const dispatch = useDispatch();
  const { id } = useParams();
  const { t } = useTranslation();
  const allMessages = useSelector(state => state.messages.messages);
  const messagesChannelId = useSelector(state => state.messages.channelId);
  const user = useSelector(state => state.auth.user);
  const channel = useSelector(state => state.channels.channels).filter(c => c._id === id)[0];
  const messagesEndRef = useRef(null);

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: 'smooth' });
  };

  useEffect(scrollToBottom, [allMessages]);

  const hasChangedChannel = messagesChannelId !== id;
  let messagesText = [];

  if (!hasChangedChannel) {
    const membersById = {};

    channel.members.forEach(m => membersById[m._id] = m);
    messagesText = allMessages.filter(m => membersById[m.user._id] !== undefined).map((m) => {
      const messageUser = membersById[m.user._id];

      return {
        id: m._id,
        senderId: messageUser._id,
        text: m.text,
        name: messageUser.firstName,
        surname: messageUser.lastName,
        primaryColor: messageUser.avatar.primaryColor,
        secondaryColor: messageUser.avatar.secondaryColor,
        channel: null,
        u: moment(m.updatedAt).format(dateTimeFormater),
      };
    });
  }

  useEffect(() => {
    if (hasChangedChannel) {
      dispatch(messages(id));
    }
  }, [dispatch, hasChangedChannel, id]);

  const myMessage = (msg) => {
    return <MyMessage value={msg.id} >
      <Popover placement="left" content= {msg.u}>
        <MyText>
          {`${msg.text}`}
        </MyText>
      </Popover>
      <div>
        <p>{t('messages.you')}</p>
      </div>
    </MyMessage>;
  };

  const otherMessage = (msg) => {
    return <Message value={msg.id} >
      <Avatar size="large" style={{
        backgroundColor: msg.primaryColor,
        verticalAlign: 'middle',
        color: msg.secondaryColor,
        marginRight: '20px',
      }}>{msg.name.charAt(0) + msg.surname.charAt(0)}</Avatar>
      <Popover placement="right" content= {msg.u}>
        <Text >
          {`${msg.text}`}
        </Text>
      </Popover>
    </Message>;
  };

  const displayMessages = messagesText.map((msg) => {
    if (msg.senderId === user._id) {
      return myMessage(msg);
    }

    return otherMessage(msg);
  });

  return (
      <Wrapper>
        {displayMessages}
        <div ref={messagesEndRef} />
      </Wrapper>
  );
};

export default Messages;
