import styled from 'styled-components';
import { color } from '../../../Constants/Colors';

export const Wrapper = styled.div`
   width: 100%;
   height: 100%;
   border-left: 1px solid whitesmoke;
   overflow: auto;
   padding: 10px;
`;

export const Title = styled.div`
  border-bottom: 1px solid whitesmoke;
  font-family: 'Roboto', sans-serif;
  border-left: 1px solid whitesmoke;
  text-align: center;
  position: static;
  top: 0;
  z-index: 100%;
`;

export const Message = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 10px;
`;

export const MyMessage = styled.div`
   display: flex;
   flex-direction: column-reverse;
   align-items: flex-end;
   margin-top:20px;
   margin-right:10px;
`;

export const Text = styled.div`
  color: white;
  width: 60%;
  border: 1px solid ${color.message};
  background: ${color.message};
  border-radius: 9px;
  text-align: center;
  margin-left: 3%;
  margin-top: 1%
  font-family: 'Roboto', sans-serif;
`;

export const MyText = styled.div`
  color: white;
  width: 60%;
  border: 1px solid ${color.mainColor};
  background: ${color.mainColor};
  border-radius: 9px;
  text-align: center;
  margin-right: 3%;
  margin-top: 1%;
  font-family: 'Roboto', sans-serif;
`;