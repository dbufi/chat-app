import React, { useState } from 'react';
import { Input } from 'antd';
import UIfx from 'uifx';
import { useParams } from 'react-router-dom';
import { SendOutlined } from '@ant-design/icons';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { sendMessage } from '../../../Core/Actions/SendMessage/SendMessageActions';
import send from '../../../Assets/send.mp3';

const SendMessageBar = () => {
  const dispatch = useDispatch();
  const [text, setText] = useState('');
  const { id } = useParams();
  const { t } = useTranslation();
  const beep = new UIfx(send);

  const handleSendMessage = () => {
    if (text.length !== 0) {
      dispatch(sendMessage(text, id));
      beep.play();
      setText('');
    }
  };

  return (
    <div style={{ width: '100%', borderTop: '1px solid whitesmoke' }}>
      <div style={{ display: 'flex', flexDirection: 'row' }}>
        <Input value={text} placeholder={t('messages.write')} style={{ width: '98%', border: '0' }} onPressEnter={handleSendMessage} onChange={e => setText(e.target.value)}/>
        <SendOutlined style={{ color: 'blue', height: '100%', marginTop: '10px' }} onClick={handleSendMessage} />
      </div>
    </div>
  );
};

export default SendMessageBar;