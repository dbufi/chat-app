import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Title } from './Styles';

const ChannelTitle = () => {
  const { id } = useParams();
  const [title, setTitle] = useState('');

  const channels = useSelector(state => state.channels.channels).filter(c => c._id === id);
  channels.forEach((t) => {
    if (title !== t.title) {
      setTitle(t.title);
    }
  });

  return (
    <Title>
      <h1>{title}</h1>
    </Title>
  );
};

export default ChannelTitle;