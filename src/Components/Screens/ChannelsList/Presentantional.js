import React, { useEffect, useState } from 'react';
import {
  Avatar, List, Badge, Modal, notification,
} from 'antd';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import EllipsisText from 'react-ellipsis-text';
import { channelList } from '../../../Core/Actions/ChannelsList/ChannelsListAction';
import { color } from '../../../Constants/Colors';
import EditChannel from '../EditChannel/Presentantional';
import {
  EditIcon, ChannelList, SearchBar, ExtraWrapper,
} from '../Styled';
import { dateTimeFormater } from '../../../Constants/DateTimeFormat';
import { device } from '../../../Constants/Devices';


const ChannelsList = () => {
  const dispatch = useDispatch();
  const [selectedChannel, setSelectedChannel] = useState({});
  const [channelFilter, setChannelFilter] = useState('');
  const [visible, setVisible] = useState(false);
  const user = useSelector(state => state.auth.user);
  const error = useSelector(state => state.channels.error);


  const { t } = useTranslation();
  const channels = useSelector(state => state.channels.channels);

  useEffect(() => {
    if (error !== null) {
      notification.error({ message: error });
    }
    dispatch(channelList());
  }, [dispatch, error]);


  const closeModal = () => {
    setVisible(false);
  };

  const openModal = () => {
    setVisible(true);
  };


  channels.forEach((c) => {
    if (c.members.length === 2) {
      c.members.forEach((m) => {
        if (m._id !== user._id) {
          c.isDirectConversation = true;
          c.title = `${m.firstName} ${m.lastName}`;

          c.user = {};
          c.user.firstName = m.firstName;
          c.user.lastName = m.lastName;
          c.user.primaryColor = m.avatar.primaryColor;
          c.user.secondaryColor = m.avatar.secondaryColor;
          c.user.active = m.active;
        }
      });
    }
  });

  const mediaQueryList = window.matchMedia(device.mobileL);

  const displayChannels = (filter) => {
    return channels.filter(c => c.title.toLowerCase().includes(filter.toLowerCase())).map((c, index) => {
      const title = c.title;
      let primaryColor = '';
      let secondaryColor = '';
      let active = '';
      let text = t('channels.data');
      let lastMessageTime = '';
      let initials = title.charAt(0);

      if (c.isDirectConversation) {
        initials = c.user.firstName.charAt(0) + c.user.lastName.charAt(0);
        primaryColor = c.user.primaryColor;
        secondaryColor = c.user.secondaryColor;
        active = c.user.active ? 'success' : 'default';
      }
      if (c.lastMessage !== undefined) {
        text = c.lastMessage.text;
        lastMessageTime = c.lastMessage.updatedAt;
        lastMessageTime = moment(lastMessageTime).format(dateTimeFormater);
      }

      const background = c._id !== selectedChannel._id ? 'white' : color.grey;

      return <List.Item
      extra={[
      <ExtraWrapper key={index}>
        <p>{lastMessageTime}</p>
        {c.adminId === user._id ? <EditIcon onClick={() => { openModal(); }} /> : null }
      </ExtraWrapper>]}
      value={c.id}
      key={index}
      style={{
        height: '50px', background, marginTop: '10px', marginLeft: '10px',
      }}
      onClick={() => setSelectedChannel(c)}>
        <Link to={c._id}>
      {!mediaQueryList.matches ? <List.Item.Meta
           avatar={
            <Badge status={active} style={{ marginTop: '10px', height: '10px', width: '10px' }}>
              <Avatar size="large" style={{
                backgroundColor: primaryColor,
                verticalAlign: 'middle',
                color: secondaryColor,
              }}>{initials}</Avatar>
            </Badge>
          }
          title={title}
          description={<EllipsisText text={text} length={19} />}
        /> : <List.Item.Meta
        avatar={
         <Badge status={active} style={{ marginTop: '10px', height: '10px', width: '10px' }}>
           <Avatar size="large" style={{
             backgroundColor: primaryColor,
             verticalAlign: 'middle',
             color: secondaryColor,
           }}>{initials}</Avatar>
         </Badge>
       }
      />}
        </Link>
      </List.Item>;
    });
  };

  return (
    <div>
      <ChannelList >
        {channels.length !== 0 ? <SearchBar
          placeholder={t('channel.search')}
          onChange={(e) => { setChannelFilter(e.target.value); }}
        /> : null}
        {displayChannels(channelFilter)}
        <Modal
          title={t('channel.new')}
          visible={visible}
          footer={false}
          onCancel={closeModal}
          >
          <EditChannel
            closeModal={closeModal}
            selectedChannelId={selectedChannel._id}
            channelsMembers={selectedChannel.members}
            selectedTitle={selectedChannel.title}
          />
        </Modal>
      </ChannelList>
    </div>
  );
};

export default ChannelsList;