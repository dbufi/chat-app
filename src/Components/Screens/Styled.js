import styled from 'styled-components';
import 'antd/dist/antd.css';
import {
  Input, Button, Select, List,
} from 'antd';
import { TwitterPicker } from 'react-color';
import { EditTwoTone } from '@ant-design/icons';
import { device } from '../../Constants/Devices';
import { color } from '../../Constants/Colors';

const { Search } = Input;

export const ColorPicker = styled(TwitterPicker)`
  margin-top: 10px;
  @media ${device.tablet} { 
    margin-left: 30px;
    width: 300px;
  }
`;

export const FormWrapper = styled.div`
  @media ${device.desktop} { 
    height: 100vh;
  }
  @media ${device.laptop} { 
    height: 120vh;
  }
`;

export const LoginText = styled.p`
  margin-left: 13px;
  margin-bottom: 0px;
`;

export const StyledInput = styled(Input)`
  margin-bottom: 20px;
  width: 389px;
  height: 46px;
  margin-left: 13px;
  margin-right: 10px;
`;

export const Image = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 50px;
`;

export const Logo = styled.div`
  display:flex;
  justify-content: center;
  margin-right:10px;
  border: 2px solid grey;
  border-radius: 5px;
  background: ${color.blue};
  width:100%;

  @media ${device.tablet} { 
    display: none;
  }
`;

export const FormInput = styled(Input)`
  border:1px solid ${color.blue};
  color: ${color.blue};
  margin-right: 10px;

  @media ${device.tablet} { 
    margin-left: 30px;
    width: 320px;
  }
`;

export const SubmitButton = styled(Button)`
  margin-bottom: 20px;
  width: 389px;
  height: 40px;
  margin-top: 30px;
  background: ${color.blue};
  color: white;

  @media ${device.tablet} { 
    margin-left: 20px;
    width: 320px;
    margin-right: 10px;
  }
`;

export const Form = styled.div`
  border: 2px solid grey;
  border-radius: 10px;
  margin-top: 25px;
  display: flex;
  justify-content: center;
  background: white;
  box-shadow: 10px 5px;

  @media ${device.mobileL} { 
    width:200px;
    margin-left: 0px;
  };

  @media ${device.tablet} { 
    margin-top: 0px;
    width: 350px;
    margin-left: 0px;
  };

  @media ${device.laptop} { 
    margin-top: 0px;
  };
  

`;

export const FormContent = styled.div`
    display: flex;
    justify-content: center;
    @media ${device.tablet} { 
      display: flex;
    }

`;

export const Text = styled.p`
  size:20px;
  margin-left:5px;
  margin-bottom: 5px;
  margin-top: 2px;

`;

export const ConfirmCard = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 350px;
  width: 400px;
  background: ${color.darkBlue};
  margin-top: 250px;
  border-radius: 10px;
  box-shadow: 10px 5px;
`;

export const MailImage = styled.img`
  width: 250px;
  height: 150px;
  margin-top: 10px;
  margin-left: 80px;
`;

export const Message = styled.h3`
  margin-left: 80px;
  color: wheat
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  margin-right: 10px;
  margin-left: 10px;
`;

export const Error = styled.h4`
  margin-left: 13px;
  margin-top: 0px;
  color: red;
  margin-bottom: 0px;
  
  @media ${device.tablet} { 
    margin-left: 30px;
  }
`;


export const ConfirmWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

export const MemberSelect = styled(Select)`
  width: 285px;
`;

export const SaveButton = styled(Button)`
  width: 100%;
  margin-top: 10px;
`;


export const EditIcon = styled(EditTwoTone)`
  align-self: flex-end;
  margin-bottom: 10px;
  margin-right: 10px;
`;

export const ChannelList = styled(List)`
  width: 356px;
  position: fixed;
  margin-bottom: 10px;
  @media ${device.mobileL} { 
    width: 188px;
  };

`;

export const SearchBar = styled(Search)`
  width: 90%; 
  margin-left: 10px;
  margin-top: 10px;
  margin-bottom: 10px;
`;

export const ExtraWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;
