import React, { useState, useEffect } from 'react';
import {
  useParams,
  Link,
} from 'react-router-dom';
import { Button, notification } from 'antd';
import { useTranslation } from 'react-i18next';
import { LoginText, StyledInput, Image } from '../Styled';
import { Wrapper } from '../../../Styles/Styles';
import { SideBar } from '../../Common/Sidebar/Styles';
import { Content } from '../../Common/Content/Styles';
import { ContentWrapper } from '../../Common/Wrapper/ContentWrapper';
import chat from '../../../Assets/Images/chat.jpg';
import logo from '../../../Assets/Images/logo.png';
import { color } from '../../../Constants/Colors';
import setNewPassword from '../../../Core/Promises';

const NewPassword = () => {
  const { id } = useParams();
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);
  const [loading, setLoading] = useState(false);
  const { t } = useTranslation();

  useEffect(() => {
    if (success !== null) {
      notification.success({ message: success });
    }
    if (error !== null) {
      notification.error({ message: error });
    }
  }, [success, error]);

  const handlePassword = (event) => {
    setPassword(event.target.value);
  };


  const sendNewPassword = () => {
    setLoading(true);
    setNewPassword(id, password).then(() => { setSuccess(t('register.passwordSuccess')); setError(null); setLoading(false); }).catch((err) => { setError(err.response.data.message); setLoading(false); });
  };

  return (
    <div >
      <ContentWrapper>
        <SideBar>
          <Wrapper>
            <Image>
              <img src={logo} style={{ marginLeft: '13px' }} alt="" />
            </Image>
            <LoginText>{t('register.password')}</LoginText>
            <StyledInput
                id="password"
                type="password"
                onChange={handlePassword}
              />
            <Button type="primary" shape="round" loading={loading} onClick={sendNewPassword} style={{ marginTop: '10px', width: '-webkit-fill-available' }}>
              {t('register.save')}
            </Button>
            <Button type="primary" shape="round" style={{
              marginTop: '10px', width: '-webkit-fill-available', background: 'white', color: 'blue',
            }}>
              <Link to="/auth/login" style={{ color: color.blue }}>
                {t('register.loginText')}
              </Link>
            </Button>
          </Wrapper>
        </SideBar>
        <Content>
          <img
            src={chat}
            style={{ width: '100%', height: '100%' }}
            alt=""
          />
        </Content>
      </ContentWrapper>
  </div>
  );
};

export default NewPassword;