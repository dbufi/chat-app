import React, { useEffect } from 'react';
import {
  Link,
} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Formik } from 'formik';
import { Button, notification } from 'antd';
import { useTranslation } from 'react-i18next';
import {
  LoginText, StyledInput, Image, Error,
} from '../Styled';
import { EmailValidation } from '../../../Constants/Validations';
import { Wrapper } from '../../../Styles/Styles';
import { SideBar } from '../../Common/Sidebar/Styles';
import { Content } from '../../Common/Content/Styles';
import { ContentWrapper } from '../../Common/Wrapper/ContentWrapper';
import chat from '../../../Assets/Images/chat.jpg';
import logo from '../../../Assets/Images/logo.png';
import { color } from '../../../Constants/Colors';
import { confirmEmail } from '../../../Core/Actions/ConfirmEmail/ConfirmEmail';

const EmailVerification = () => {
  const loading = useSelector(state => state.verifyEmail.loading);
  const error = useSelector(state => state.verifyEmail.error);
  const success = useSelector(state => state.verifyEmail.response);
  const { t } = useTranslation();

  const dispatch = useDispatch();

  useEffect(() => {
    if (error !== null) {
      notification.error({ message: error });
    }
    if (success !== null) {
      notification.success({ message: t('register.checkEmail') });
    }
  }, [error, success, t]);

  return (
    <div>
      <Formik
        initialValues={{ email: '' }}
        onSubmit={(values) => {
          dispatch(confirmEmail(values.email));
        }}
        validationSchema={EmailValidation(t)}
      >
      {(formProps) => {
        const {
          values,
          touched,
          errors,
          handleChange,
          handleSubmit,
        } = formProps;

        return (
          <div >
            <ContentWrapper>
              <SideBar>
                <Wrapper>
                  <Image>
                    <img src={logo} style={{ marginLeft: '13px' }} alt="" />
                  </Image>
                  <LoginText>{t('register.email')}</LoginText>
                  <StyledInput
                    id="email"
                    type="text"
                    value={values.email}
                    onChange={handleChange}
                    className={
                      errors.email && touched.email
                        ? 'text-input error'
                        : 'text-input'
                      }
                    />
                    <Error>
                      {errors.email && touched.email && (
                      <div className="input-feedback">{errors.email}</div>
                      )}
                    </Error>
                  <Button type="primary" shape="round" loading={loading} onClick={handleSubmit} style={{ marginTop: '10px', width: '-webkit-fill-available' }}>
                    {t('register.sent')}
                  </Button>
                  <Button type="primary" shape="round" style={{
                    marginTop: '10px', width: '-webkit-fill-available', background: 'white', color: 'blue',
                  }}>
                    <Link to="/auth/login" style={{ color: color.blue }}>
                      {t('register.loginText')}
                    </Link>
                  </Button>
                </Wrapper>
              </SideBar>
              <Content>
                <img
                  src={chat}
                  style={{ width: '100%', height: '100%' }}
                  alt=""
                />
              </Content>
            </ContentWrapper>
          </div>
        );
      }}
      </Formik>
    </div>
  );
};


export default EmailVerification;