import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Select, Avatar,
} from 'antd';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { usersList } from '../../../Core/Actions/UsersList/UsersListAction';
import { MemberSelect } from '../Styled';


const UsersList = ({ setUserIds, channelsMembers, channelId }) => {
  const dispatch = useDispatch();
  const [selectedChannelId, setSelectedChannelId] = useState('');
  const [selectedMemberIds, setSelectedMemberIds] = useState([]);

  const { t } = useTranslation();
  useEffect(() => {
    dispatch(usersList());
  }, [dispatch]);


  const user = useSelector(state => state.auth.user);


  const { Option } = Select;
  const users = useSelector(state => state.users.users);

  const usersData = users.map(person => ({
    name: person.firstName,
    surname: person.lastName,
    primaryColor: person.avatar.primaryColor,
    secondaryColor: person.avatar.secondaryColor,
    nameInitial: person.firstName.charAt(0),
    surnameInitial: person.lastName.charAt(0),
    id: person._id,
  }));

  if (channelId !== selectedChannelId) {
    const ids = channelsMembers.map(c => c._id);
    // current user is always the last entry
    ids.pop();
    setSelectedChannelId(channelId);
    setSelectedMemberIds(ids);
  }

  const handleSelectedUsers = (value) => {
    setUserIds(value);
    setSelectedMemberIds(value);
  };


  const usersListData = usersData.filter(person => person.id !== user._id);

  const displayData = () => {
    return usersListData.map((userData, index) => (
      <Option value={userData.id} key={index} style={{ height: '50px' }}>
        <Avatar size="small" style={{
          backgroundColor: userData.primaryColor,
          verticalAlign: 'middle',
          color: userData.secondaryColor,
          marginRight: '20px',
        }}>{userData.nameInitial + userData.surnameInitial}</Avatar>
        {`${userData.name} ${userData.surname}`}
      </Option>
    ));
  };

  return (
    <div>
      <MemberSelect allowClear={true} showSearch={false} mode="multiple" placeholder={t('channel.members')} onChange={handleSelectedUsers} value={selectedMemberIds} >
        {displayData()}
      </MemberSelect>
    </div>
  );
};

UsersList.propTypes = {
  setUserIds: PropTypes.func,
  channelsMembers: PropTypes.array,
  channelId: PropTypes.string,
};

export default UsersList;